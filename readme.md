# Diversification history of Neotropical Lecythidaceae, an ecologically dominant tree family of Amazon rain forest  #

This is the repository for the book chapter "Diversification history of Neotropical Lecythidaceae, an ecologically dominant tree family of Amazon rain forest" published in the book "Neotropical Diversiication", edited by Carnaval and Rull.

The repository is organized in three main folders comprising the three main analysis presented in the paper.

Iniside each one of the folders you will find the code used for every component of the pipeline and the main results

Please cite as:
Vargas OM, Dick CW. Diversification history of Neotropical Lecythidaceae, an ecologically dominant tree family of Amazon rain forest, in Rull V. Neotropical Diversification. Springer. 



