
dir()
#install.packages("phytools")
#install.packages("maps")
library(maps)
library(phytools)
library(ape)

MaxCred <- read.nexus("chronogram_v2.tre")

plot(MaxCred, cex=0.5, show.node.label=T)
axisPhylo()

#next command remove the outgroups
MaxCredNO <- drop.tip(MaxCred, c("Barringtonia_acutangula", "Barringtonia_asiatica", "Barringtonia_edulis", "Barringtonia_fusicarpa", "Barringtonia_norshamiae", "Barringtonia_pendula", "Barringtonia_racemosa", "Scytopetalum_klaineanum"))
plot(MaxCredNO, cex=0.5, show.node.label=T)
axisPhylo()

MaxCredNOLad <- ladderize(MaxCredNO)
plot(MaxCredNOLad, cex=0.5, show.node.label=T)
axisPhylo()

par(mfcol=c(1,2))
plot(MaxCred, cex=0.5)
axisPhylo()
plot(MaxCredNOLad, cex=0.5)
axisPhylo()

write.tree(MaxCredNOLad, file="chronogram_v2_no.tree")


