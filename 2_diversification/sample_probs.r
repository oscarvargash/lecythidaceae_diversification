#Script to create the sampleing probability file required by BAMM

dir()
#install.packages("pastis")
library(pastis)
library(phytools)
library(dplyr)

#first lets extract the names of the taxa in three
tree <- read.tree("chronogram_v1.2_no.tree")
plot(tree, cex=0.5, show.node.label=T)
axisPhylo()
taxa <- c(tree$tip.label)
taxa2 <- gsub("_", " ", taxa)
str(taxa2)

#Second let's count the number of species per group from 
#the database with valid species names in the Lecythidaceae
species <- read.csv("Lecythidaceae_accepted_names_v2.csv")
head(species)

#keep only species
species2 <- species %>% filter(sp=="Y")
head(species2)
clades <- species2 %>% group_by(clade) %>% filter(clade!="Unknown") %>% tally
clades

#Make the comparison
taxa_sampled <- filter(species2, binomial %in% taxa2) 

#check diff between list to understand which species have mistakes
taxa2 <- as.data.frame(taxa2)
sp_w_mistakes <- setdiff(taxa2$taxa2, taxa_sampled$binomial)

#when the result is 0, a calculatoin of the number of species per clade can be made
clades_sampled <- taxa_sampled %>% group_by(clade) %>% tally
names(clades_sampled)[2]<-paste("sampled")

#merge the tables
clades_sampled2 <- merge(clades, clades_sampled)
clades_sampled2 = mutate(clades_sampled2, prob = (sampled / n))

#add the probability to the table by merging the two tables
taxa_sampled2 <- merge(taxa_sampled, clades_sampled2)

#save the table as a comma separated file
write.csv(taxa_sampled2, file= "taxa_sammpled_probs.csv", fileEncoding = "UTF-8")

### Adding at random inside their clades, species missing from the phylogeny
###
#Select ony the neotrpoical species out the valid species list
nw_species <- species2 %>% filter(clade!="Unknown")
write.csv(nw_species, file= "nw_species.csv", fileEncoding = "UTF-8")

#compute the difference between species sampled and the nw_species
sp_tree_add <-  setdiff(nw_species$binomial, taxa_sampled$binomial)

#obtain a subselection of the species to add from the nw_species table
sp_tree_add2 <- filter(nw_species, binomial %in% sp_tree_add)

#replace underline with space
sp_tree_add2$binomial <- gsub(" ", "_", sp_tree_add2$binomial)
write.csv(nw_species, file= "species_missing_tree.csv", fileEncoding = "UTF-8")

#look at three node for adding taxa
plot(tree, cex=0.5, show.node.label=T)
axisPhylo()
nodelabels(width = 0.5, height = 0.5, cex=0.5)

#subset species to be added
for (i in (clades$clade)) {
  label <- paste("",i,sep = "")
  print(label)
  assign(label, sp_tree_add2 %>% filter(clade == i) %>% pull(binomial))
}

#lets just check that the subsetting was done by typing the clades
Bertholletia #this one has no taxa
Carinaria
Chartacea
Corrugata #this one has no taxa
Corythophora #this one has no taxa
Couratari
Couroupita #this one has no taxa
Grias
Gustavia
Integrifolia
Ollaria #this one has no taxa
Parvifolia
Pisonis
Poiteaui
Tetrapetala

#now, it is time to add the taxa to the tree
tree2 <- add.random(tree, tips="DONDONDONDON", edge.length=NULL, is.ultrametric(tree) ==TRUE)
plot(tree2, cex=0.5)
#^ adding tips using this method does not produce good results as the tip is added randomly
#^ and with bind.tip the tip is not added randomly

#testing PASTIS

# create the table with all the taxa that is going to be in located in the tree
#subselect from species2
species3 <- species2 %>% filter(clade!="Unknown") %>% dplyr::select("binomial","clade")
species3$binomial <- gsub(" ", "_", species3$binomial)
names(species3)[1]<-paste("taxon")
str(species3)
write.csv(species3, file= "species_complete_tree.csv", fileEncoding = "UTF-8")

#defining PASTIS files:
pastis_main(constraint_tree=tree, taxa_list="species_complete_tree.csv", output_file="lecy.nexus")

conch(constraint_tree=tree, mrbayes_output="lecy.nexus.t", simple_edge_scaling=TRUE)

#PASTIS SUCKS as it burns down the time calibration, also the model to add the branches
#would clearly bias the 
